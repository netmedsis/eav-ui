export interface EmptyMessageTemplateVars {
  notes: string;
  visible: boolean;
}
