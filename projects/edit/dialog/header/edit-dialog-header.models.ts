import { PublishMode } from '../../shared/models';

export interface EditDialogHeaderTemplateVars {
  hasLanguages: boolean;
  publishMode: PublishMode;
}
