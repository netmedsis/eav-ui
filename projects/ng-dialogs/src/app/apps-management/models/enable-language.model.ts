export interface EnableLanguage {
  Code: string;
  Culture: string;
  IsEnabled: boolean;
}
