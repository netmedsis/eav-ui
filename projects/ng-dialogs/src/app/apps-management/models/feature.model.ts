export interface Feature {
  enabled: boolean;
  expires: string;
  id: string;
  public: boolean;
  ui: boolean;
}
