export interface App {
  Id: number;
  IsApp: boolean;
  Guid: string;
  Name: string;
  Folder: string;
  AppRoot: string;
  IsHidden: boolean;
  ConfigurationId: number;
  Items: number;
  Thumbnail: string;
  Version: string;
}
