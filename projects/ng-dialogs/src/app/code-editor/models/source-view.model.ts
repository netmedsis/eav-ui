export interface SourceView {
  AppId: number;
  Code: string;
  Extension: string;
  FileName: string;
  HasApp: boolean;
  HasList: boolean;
  IsSafe: boolean;
  LocationScope: string;
  Name: string;
  Streams: Record<string, any>;
  Type: string;
  TypeContent: string;
  TypeContentPresentation: string;
  TypeList: string;
  TypeListPresentation: string;
}
