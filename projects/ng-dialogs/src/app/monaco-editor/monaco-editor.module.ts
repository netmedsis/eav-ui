import { NgModule } from '@angular/core';
import { MonacoEditorComponent } from './monaco-editor.component';

@NgModule({
  declarations: [MonacoEditorComponent],
  exports: [MonacoEditorComponent],
})
export class MonacoEditorModule { }
