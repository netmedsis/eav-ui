export interface GroupHeader {
  Index: number;
  Id: number;
  Guid: string;
  Title: string;
  Type: string;
}
